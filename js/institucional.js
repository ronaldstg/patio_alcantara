jQuery(document).ready(function($) {
	
	$(".bloco-imagens ul li a").click(function() {

		var page_alt = ($("#lightbox").height());
		var position = (page_alt/2);

		$("#lightbox .foto").css("top", (position/2) + "px");
		

		var obj = $(this);
		var url = obj.attr("data-url");

		$("#lightbox").fadeIn();
		$("#lightbox .foto .item").html("<img src='img/institucional/" + url +"' />");
		//$("#lightbox .foto .item").append("<a href='#' class='close'>x</a>");				

		var position_obj = $("#lightbox .foto .item img").position();
		var pos_left = position_obj.left;
		
		var pos_close = (pos_left + ($("#lightbox .foto .item img").width())) + 20;

		$(".close").css("left", pos_close);

		return false;

	});

	$(".close").click(function() {
		$("#lightbox .foto .item").html("");
		$("#lightbox").fadeOut();
	});

});
